def test_basic(logfile, cli):
    cli(
        "--log-plugin=basic",
        "--log-level=trace",
        f"--log-file={logfile}",
        "--log-handler-options",
        "mode=a",
    )
    contents = logfile.read_text()
    assert "[TRACE   ] trace\n" in contents


def test_async(logfile, cli):
    cli(
        "--log-plugin=async",
        "--log-level=trace",
        f"--log-file={logfile}",
        "--log-handler-options",
        "mode=a",
    )
    contents = logfile.read_text()
    assert "[TRACE   ] trace\n" in contents


def test_null(cli):
    stderr = cli(
        "--log-plugin=null",
        "--log-level=trace",
    )
    assert "[TRACE   ] trace\n" in stderr


def test_loaded_mod_ref(cli):
    contents = cli("--help", check=False)
    assert "timed_rotating" in contents
    assert "rotating" in contents
    assert "socket" in contents
    assert "basic" in contents
    assert "async" in contents
    assert "socket" in contents
