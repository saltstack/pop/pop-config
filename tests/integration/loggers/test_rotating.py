import os

import pytest


@pytest.mark.skipif(os.name == "nt", reason="This test doesn't work on windows")
def test_rotating(logfile, cli):
    cli(
        "--log-plugin=rotating",
        "--log-level=trace",
        f"--log-file={logfile}",
        "--log-handler-options",
        "mode=a+",
        "maxBytes=10",
        "backupCount=1",
    )

    with open(f"{logfile}.1") as fh:
        contents = fh.read()
    assert "[TRACE   ] trace\n" in contents


@pytest.mark.skipif(os.name == "nt", reason="This test doesn't work on windows")
def test_async_rotating(logfile, cli):
    cli(
        "--log-plugin=rotating",
        "--log-level=trace",
        f"--log-file={logfile}",
        "--log-handler-options",
        "mode=a",
        "maxBytes=10",
        "backupCount=1",
    )

    with open(f"{logfile}.1") as fh:
        contents = fh.read()

    assert "[TRACE   ] trace\n" in contents
