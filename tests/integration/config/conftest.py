import json
import os
import pathlib
import subprocess
import sys

import dict_tools.data as data
import pytest


def run_cli(*args, load, env: dict = None, check: bool = True):
    if env is None:
        env = {}

    if os.name == "nt" and "SYSTEMROOT" not in env:
        env["SYSTEMROOT"] = os.getenv("SYSTEMROOT")

    # Patch the python path to have the testing dyne directory
    env["PYTHONPATH"] = str(pathlib.Path(__file__).parent.parent.parent / "pypath")
    env["TEST_LOAD_STR"] = json.dumps(load)

    runpy = pathlib.Path(__file__).parent / "run.py"
    command = [sys.executable, str(runpy), *args]

    proc = subprocess.Popen(
        command,
        encoding="utf-8",
        env=env,
        stderr=subprocess.PIPE,
        stdout=subprocess.PIPE,
    )
    wait = proc.wait()
    stdout = proc.stdout
    if not isinstance(stdout, str):
        stdout = stdout.read()
    stderr = proc.stderr
    if not isinstance(stderr, str):
        stderr = stderr.read()
    if check:
        assert wait == 0, stderr
        return data.NamespaceDict(json.loads(stdout))
    else:
        return stdout or stderr


@pytest.fixture(scope="session")
def cpath_dir():
    cpath = pathlib.Path(__file__).parent.parent.parent / "config"
    assert cpath.exists()
    yield cpath


@pytest.fixture(name="cli", scope="function")
def cli():
    return run_cli
