def test_ordering_1(cli, cpath_dir):
    OPT = cli(
        "--config",
        str(cpath_dir / "full.conf"),
        "--alls",
        "cli",
        "--oscli",
        "cli",
        load=(["full", "d1", "m1", "m2"], "full", ["d1"]),
        env={"OS_ALONE": "os", "OS_CLI": "os", "ALLS": "os", "OS_BASE_SOURCE": "os"},
    )
    assert OPT.m2.base == "os"
    assert OPT.full.alls == "cli"
    assert OPT.full.oscli == "cli"
    assert OPT.full.osalone == "os"
    assert OPT.d1.exdyned1 == "default"
    assert OPT.d1.exdyned2 == "default"
    assert OPT.d1.exdyned3 == "default"
    assert OPT.d1.exdyned4 == "default"
