===========
Mod Choices
===========

"loaded_mod_choices_ref" can be added to the CLI_CONFIG.
This reads the loaded modules at the given ref, and then extends the CLI option
with "choices" based on available loaded mods.

For example, in pop-config itself, the log-plugin has this option specified:

.. code-block:: python

    CLI_CONFIG = {"log_plugin": {"loaded_mod_choices_ref": "log"}}


Previous --help output:

.. code-block::

      --log-plugin LOG_PLUGIN
                            The logging plugin to use

New --help output:

.. code-block::

      --log-plugin {init,queue,event,async,async_timed_rolling,basic,datagram,null,rotating,socket,timed_rotating}
                            The logging plugin to use
