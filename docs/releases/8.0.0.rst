=====
8.0.0
=====

What's new in version 8?

- The code has been broken up into reasonable chunks so that it is more readable and maintainable.
- The previously nested dyne of "config" has been split into 4 pluggable dynes.
- Global subcommands, such as the logging parameters, now show up properly grouped under the root parser and subparsers.
- Many logging plugins have been added to the "log" dyne

args
====

The "args" dyne contains setup and configuration of argparse's ArgumentParser.
This could potentially be hotswapped for a different argument parsing method.

comps
=====

The "comps" dyne contains various parsing plugins that are applied to the arg parser.
Each ``comps`` plugin has a single "get" function that gathers grouping and argument parameter data.

Example:

.. code-block:: python


    def get(
        hub,
        # The entire raw cli definition
        raw_cli: Dict[str, Any],
        # The current argument being examined
        arg: str,
    ) -> Dict[str, Any]:
        # The raw cli definition that applies to this argument
        comps = raw_cli[arg]

        result = {
            # kwargs that will be passed to the "add_argument" operation for this argument
            "parser_kwargs": {},
            # args that will be passed to the "add_argument" operation for this argument
            "parser_args": [],
            # Groups that this argument should be added to
            "groups": {},
            # Mutually Exclusive groups that this argument should be added to
            "ex_groups": {},
            # Subcommands that this argument should be added to
            "subcommands": [],
        }

        return result

render
======

The "render" dyne contains plugins that are useful for parsing config files and cli args.

log
===

The "log" dyne was greatly expanded to include loggers with alternate handlers.

basic
-----

The default logger that uses a filehandler based on the app name.
Writes formatted logging records to disk files.

Usage:

.. code-block:: bash

    $ pop-app --log-plugin=basic --log-handler-options mode=a encoding=None --log-file=app_name.log

datagram
--------

A logger which writes logging records, in pickle format, to
a datagram socket.  The pickle which is sent is that of the LogRecord's
attribute dictionary (__dict__), so that the receiver does not need to
have the logging module installed in order to process the logging event.

Usage:

.. code-block:: bash

    $ pop-app --log-plugin=datagram --log-file=127.0.0.1:8080

nt_event
--------

A handler class which sends events to the NT Event Log. Adds a
registry entry for the specified application name. If no dllname is
provided, win32service.pyd (which contains some basic message
placeholders) is used. Note that use of these placeholders will make
your event logs big, as the entire message source is held in the log.
If you want slimmer logs, you have to pass in the name of your own DLL
which contains the message definitions you want to use in the event log.

Usage:

.. code-block:: bash

    $ pop-app --log-plugin=nt_event --log-handler-options appname=my_app dllname=None logtype=Application

null
----

A logger which does not log to a file, just to the console

Usage:

.. code-block:: bash

    $ pop-app --log-plugin=null

rotating
--------

Logging plugin for logging to a set of files, which switches from one file
to the next when the current file reaches a certain size.

Usage:

.. code-block:: bash

    $ pop-app --log-plugin=rotating --log-file=my_app.log --log-handler-options mode=a maxBytes=0 backupCount=0 encoding=None

socket
------

Initializes the handler with a specific host address and port.

When the attribute *closeOnError* is set to True - if a socket error
occurs, the socket is silently closed and then reopened on the next
logging call.

Usage:

.. code-block:: bash

    $ pop-app --log-plugin=socket --log-file=127.0.0.1:8080

timed_rotating
--------------

Plugin for logging to a file, rotating the log file at certain timed
intervals.

If backupCount is > 0, when rollover is done, no more than backupCount
files are kept - the oldest ones are deleted.

Usage:

.. code-block:: bash

    $ pop-app --log-plugin=timed_rotating --log-file=my_app.log --log-handler-options when=h interval=1 backupCount=0 encoding=None utc=False atTime=None

async
-----

A logger that uses asynchronous operations.

Your code should create a loop on the hub before making calls to the logger.

.. code-block:: python

    hub.pop.loop.create()

The async extras for pop-config also need to be installed to enable the plugin.

.. code-block:: bash

    $ pip install pop-config[async]

Usage:

.. code-block:: bash

    $ pop-app --log-plugin=async --log-handler-options mode=a encoding=None --log-file=app_name.log

async_timed_rolling
-------------------

Plugin for logging to a file, rotating the log file at certain timed
intervals.

If ``backup_count`` is > 0, when rollover is done, no more than ``backup_count``
files are kept - the oldest ones are deleted.


.. list-table:: when/at_then options
   :widths: 25 150
   :header-rows: 1

   * - when
     - at_time behavior
   * - SECONDS
     - at_time will be ignored
   * - MINUTES
     - -- // --
   * - HOURS
     - -- // --
   * - DAYS
     - at_time will be IGNORED. See also MIDNIGHT
   * - MONDAYS
     - rotation happens every WEEK on MONDAY at ${at_time}
   * - TUESDAYS
     - rotation happens every WEEK on TUESDAY at ${at_time}
   * - WEDNESDAYS
     - rotation happens every WEEK on WEDNESDAY at ${at_time}
   * - THURSDAYS
     - rotation happens every WEEK on THURSDAY at ${at_time}
   * - FRIDAYS
     - rotation happens every WEEK on FRIDAY at ${at_time}
   * - SATURDAYS
     - rotation happens every WEEK on SATURDAY at ${at_time}
   * - SUNDAYS
     - rotation happens every WEEK on SUNDAY at ${at_time}
   * - MIDNIGHT
     - rotation happens every DAY at ${at_time}

Your code should create a loop on the hub before making calls to the logger.

.. code-block:: python

    hub.pop.loop.create()

The async extras for pop-config also need to be installed to enable the plugin.

.. code-block:: bash

    $ pip install pop-config[async]

Usage:

.. code-block:: bash

    $ pop-app --log-plugin=async_timed_rolling --log-file=app_name.log --log-handler-options when=MIDNIGHT interval=1 backup_count=10 encoding=None utc=None at_time=None
